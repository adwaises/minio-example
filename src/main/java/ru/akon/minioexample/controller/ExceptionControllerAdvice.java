package ru.akon.minioexample.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.akon.minioexample.dto.ErrorResponseDto;

import java.io.FileNotFoundException;

@ControllerAdvice
@Slf4j
public class ExceptionControllerAdvice {

    @ExceptionHandler(FileNotFoundException.class)
    public ResponseEntity<ErrorResponseDto> noResultExceptionHandler(Exception ex) {
        log.warn(ex.getMessage());
        ErrorResponseDto error = new ErrorResponseDto();
        error.setErrorCode(HttpStatus.NOT_FOUND.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}