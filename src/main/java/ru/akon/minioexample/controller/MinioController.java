package ru.akon.minioexample.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.akon.minioexample.service.MinioService;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(value = "api/minio/")
public class MinioController {

    private final MinioService minioService;

    @ApiOperation("Загрузка файла")
    @PostMapping("upload")
    public String upload(@RequestParam("file") MultipartFile file,
                         @RequestParam(value = "path", required = false) String path) {
        return minioService.upload(file, path);
    }

    @ApiOperation("Получение файла")
    @GetMapping("/download")
    public ResponseEntity<ByteArrayResource> download(@RequestParam(name = "object-name") String objectName) {
        byte[] data = minioService.download(objectName);
        ByteArrayResource resource = new ByteArrayResource(data);

        return ResponseEntity
                .ok()
                .contentLength(data.length)
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + objectName + "\"")
                .body(resource);
    }

}
