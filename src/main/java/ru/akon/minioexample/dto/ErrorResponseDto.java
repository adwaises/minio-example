package ru.akon.minioexample.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "ErrorResponseDto", description = "Ошибка")
public class ErrorResponseDto {
    @ApiModelProperty(value = "Код ошибки")
    private Integer errorCode;
    @ApiModelProperty(value = "Сообщение")
    private String message;
}
