package ru.akon.minioexample.service;

import org.springframework.web.multipart.MultipartFile;

public interface MinioService {
    String upload(MultipartFile file, String path);
    byte[] download(String objectName);
}
