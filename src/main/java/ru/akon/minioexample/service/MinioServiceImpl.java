package ru.akon.minioexample.service;

import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.InputStream;

@Service
@Slf4j
@RequiredArgsConstructor
public class MinioServiceImpl implements MinioService {

    @Value("${minio.bucketname}")
    private String bucketName;

    private final MinioClient minioClient;

    @Override
    @SneakyThrows
    public String upload(MultipartFile file, String path) {

        String fileName = file.getOriginalFilename();

        if (path != null && !path.endsWith("/") && !path.endsWith("\\")) {
            path += "/";
        }

        ObjectWriteResponse response = minioClient.putObject(PutObjectArgs.builder()
                .bucket(bucketName)
                .object(path != null ? (path + fileName) : fileName)
                .stream(file.getInputStream(), file.getSize(), -1)
                .build());

        log.info("'{}' is successfully uploaded as "
                + "object '{}' to bucket '{}'.", fileName, fileName, bucketName);

        return "Файл успешно загружен, путь к файлу: " + response.object();
    }


    @Override
    @SneakyThrows
    public byte[] download(String objectName) {

        try (InputStream stream = minioClient.getObject(
                GetObjectArgs.builder()
                        .bucket(bucketName)
                        .object(objectName)
                        .build())) {

            return IOUtils.toByteArray(stream);
        } catch (Exception e) {
            String message = "Не удалось найти файл: " + objectName;
            log.warn(message, e);
            throw new FileNotFoundException(message);
        }
    }
}
